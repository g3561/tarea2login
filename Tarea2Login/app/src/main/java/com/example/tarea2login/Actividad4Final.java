package com.example.tarea2login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class Actividad4Final extends AppCompatActivity {

    TextView textViewUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad4_final);

        textViewUsuario = findViewById(R.id.textViewUsuario);

        Intent i = getIntent();
        String nombreUsuario = i.getStringExtra("nombreUsuario");

        if (nombreUsuario != null) {
            textViewUsuario.setText(nombreUsuario);
        } else {
            textViewUsuario.setText("Error, no se encontró ningún nombre de usuario");
        }
    }
}