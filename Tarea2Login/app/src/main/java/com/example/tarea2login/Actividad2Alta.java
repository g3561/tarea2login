package com.example.tarea2login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Actividad2Alta extends AppCompatActivity implements View.OnClickListener {

    EditText editTextNombre, editTextContraseña, editTextRepetir, editTextCorreo;
    Button buttonAceptar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad2_alta);

        buttonAceptar = findViewById(R.id.buttonAceptar);

        editTextNombre = findViewById(R.id.editTextNombre);
        editTextContraseña = findViewById(R.id.editTextContraseña);
        editTextRepetir = findViewById(R.id.editTextRepetir);
        editTextCorreo = findViewById(R.id.editTextCorreo);

        buttonListeners();
    }

    private void buttonListeners() {
        buttonAceptar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (!editTextNombre.getText().toString().equals("") &&
                !editTextContraseña.getText().toString().equals("") &&
                !editTextRepetir.getText().toString().equals("") &&
                !editTextCorreo.getText().toString().equals("")) {
            if (editTextContraseña.getText().toString().equals(editTextRepetir.getText().toString())) {
                String nombre = editTextContraseña.getText().toString();
                String contraseña = editTextRepetir.getText().toString();
                Intent i = new Intent();
                i.putExtra("Nombre", nombre);
                i.putExtra("Contraseña", contraseña);
                setResult(RESULT_OK, i);
                Actividad2Alta.super.finish();
            } else {
                Toast.makeText(getApplicationContext(),
                        "La contraseña y su repetición deben coincidir.",
                        Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(),
                    "Debe de completar todos los campos para darse de alta.",
                    Toast.LENGTH_LONG).show();
        }
    }
}