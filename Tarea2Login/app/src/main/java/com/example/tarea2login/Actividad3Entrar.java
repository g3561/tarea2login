package com.example.tarea2login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Actividad3Entrar extends AppCompatActivity implements View.OnClickListener {

    EditText editTextNombre, editTextContraseña;
    Button buttonAceptar;
    Usuarios users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad3_entrar);

        buttonAceptar = findViewById(R.id.buttonAceptarEntrar);

        editTextNombre = findViewById(R.id.editTextNombreEntrar);
        editTextContraseña = findViewById(R.id.editTextContraseñaEntrar);

        buttonListeners();

        users = (Usuarios) getIntent().getSerializableExtra("Usuarios");
    }

    private void buttonListeners() {
        buttonAceptar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (!editTextNombre.getText().toString().equals("")
                && !editTextContraseña.getText().toString().equals("")) {
            if (users.accesoUsuario(editTextNombre.getText().toString(), editTextContraseña.getText().toString())) {
                Intent i = new Intent(getBaseContext(), Actividad4Final.class);
                i.putExtra("nombreUsuario", editTextNombre.getText().toString());
                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(),
                        "Los datos introducidos no se corresponden con ningún registro.",
                        Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(),
                    "Debe de completar todos los campos para acceder.",
                    Toast.LENGTH_LONG).show();
        }
    }
}