package com.example.tarea2login;


import java.io.Serializable;
import java.sql.SQLOutput;
import java.util.ArrayList;

public class Usuarios implements Serializable {
    public class Usuario implements Serializable {
        private String nombre;
        private String password;

        public Usuario(String nombre, String password) {

            this.nombre = nombre;
            this.password = password;
        }

        public String getNombre() {
            return nombre;
        }

        public String getPassword() {
            return password;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    private ArrayList<Usuario> lista = new ArrayList<Usuario>();

    public void insertaUsuario(String nombre, String pass) {
        Usuario user = new Usuario(nombre, pass);
        lista.add(user);
    }

    public boolean accesoUsuario(String nombre, String pass) {
        for (Usuario u : lista) {
            if (u.getNombre().equals(nombre) && u.getPassword().equals(pass)) {
                return true;
            }
        }
        return false;
    }
}
