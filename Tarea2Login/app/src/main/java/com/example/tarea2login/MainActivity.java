package com.example.tarea2login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    final int CODIGO_LLAMADA = 14;

    Button botonEntrar, botonAlta;
    ToggleButton toggle;

    Usuarios users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonEntrar = findViewById(R.id.buttonEntrar);
        botonAlta = findViewById(R.id.buttonAlta);

        toggle = findViewById(R.id.toggleButtonUsuarioInvitado);

        buttonListeners();

        users = new Usuarios();
    }

    private void buttonListeners() {
        botonEntrar.setOnClickListener(this);
        botonAlta.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonEntrar:
                if (toggle.isChecked()) {
                    Intent i = new Intent(getBaseContext(), Actividad4Final.class);
                    i.putExtra("nombreUsuario", "Usuario invitado");
                    startActivity(i);
                } else {
                    Intent i = new Intent(getBaseContext(), Actividad3Entrar.class);
                    i.putExtra("Usuarios", users);
                    startActivity(new Intent(i));
                }
                break;
            case R.id.buttonAlta:
                startActivityForResult(
                        new Intent(this, Actividad2Alta.class),
                        CODIGO_LLAMADA);
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent i) {
        super.onActivityResult(requestCode, resultCode, i);
        if (requestCode == CODIGO_LLAMADA && resultCode == RESULT_OK) {
            if (i.hasExtra("Nombre") && i.hasExtra("Contraseña")) {
                String nombre = i.getStringExtra("Nombre");
                String contraseña = i.getStringExtra("Contraseña");
                users.insertaUsuario(nombre, contraseña);
            }
        }
    }
}